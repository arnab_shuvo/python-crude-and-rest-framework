$(document).on('submit', 'form.insert', function (e) {
    e.preventDefault();
    const form = $('.insert');
    $.ajax({
        url: '/insert',
        method: 'POST',
        data: form.serialize(),
        dataType: 'json',
        success: function (data) {

        }
    });
});