from django.db import models


class Person(models.Model):

    name = models.CharField(max_length=56)
    age = models.IntegerField(null=True)

    class Meta:
        db_table = "persons"
