from django.urls import path
from .views import *
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('insert', csrf_exempt(insert)),
    path('', index),
    path('edit/<int:person_id>', csrf_exempt(edit)),
    path('delete/<int:person_id>', delete),
]