from django.shortcuts import render, HttpResponse, redirect
from demo.models import Person
import json


def index(request):
    persons = Person.objects.filter()
    return render(request, 'index.html', {'persons': persons})


def edit(request, person_id):
    if request.method != 'POST':
        persons = Person.objects.get(id=person_id)
        return render(request, 'edit.html', {'persons': persons})
    else:
        person_id = request.POST.get('id', '')
        persons = Person.objects.get(id=person_id)
        persons.name = request.POST.get('name', '').strip()
        persons.age = request.POST.get('age', '').strip()

        if len(persons.name) > 0 and len(persons.age) > 0:
            persons.save()
            return redirect(to='/')

    return render(request, 'edit.html', {'persons': persons})


def insert(request):
    if request.method == 'POST':
        name = request.POST.get('name', '').strip()
        age = request.POST.get('age', '').strip()
        if len(name) > 0 and len(age) > 0:
            Person(name=name, age=age).save()
            response = {
                'status': True
            }
            return HttpResponse(json.dumps(response), content_type='application/json')

    return render(request, 'insert.html')


def delete(request, person_id):
    persons = Person.objects.get(id=person_id)
    persons.delete()
    return redirect(to='/')
