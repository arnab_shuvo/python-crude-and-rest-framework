from rest_framework import serializers
from api.models import Student
from demo.models import Person


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('name', 'age', 'person')

