from django.db import models
from demo.models import Person


class Student(models.Model):

    name = models.CharField(max_length=56)
    age = models.IntegerField()
    person = models.ForeignKey(to=Person, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = "students"


class Category(models.Model):

    name = models.CharField(max_length=56, unique=True)
    description = models.CharField(max_length=500)

    class Meta:
        db_table = "category"


class Product(models.Model):
    name = models.CharField(max_length=56, unique=True)
    description = models.CharField(max_length=500)
    category = models.ForeignKey(to=Category, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = "products"

