from rest_framework.routers import SimpleRouter
from api.views import StudentViewSet
from api.views import ProductViewSet
from api.views import CategoryViewSet

router = SimpleRouter()
router.register('students', StudentViewSet)
router.register('products', ProductViewSet)
router.register('category', CategoryViewSet)
